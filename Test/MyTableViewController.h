//
//  MyTableViewController.h
//  Test
//
//  Created by Roman on 24.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewController : UITableViewController   <NSXMLParserDelegate>

@property (nonatomic, retain) NSString *nameFileList;
@property (nonatomic, retain) NSMutableArray *categoriesList;
@property (strong, nonatomic) IBOutlet UITableView *categoryListTableView;

@property (nonatomic, retain) NSMutableData *rssData;



//categories
@property (nonatomic, retain) NSMutableArray *categories;
@property (nonatomic, retain) NSString * currentElement;
@property (nonatomic, retain) NSMutableString *currentTitle;
@property (nonatomic, retain) NSMutableString *id_category;
@property (nonatomic, retain) NSMutableString *category;

//offers
@property (nonatomic, retain) NSMutableArray *offers;
@property (nonatomic, retain) NSMutableString *offer_id;
@property (nonatomic, retain) NSMutableString *url;
@property (nonatomic, retain) NSMutableString *name;
@property (nonatomic, retain) NSMutableString *price;
@property (nonatomic, retain) NSMutableString *offer_description;
@property (nonatomic, retain) NSMutableString *picture;
@property (nonatomic, retain) NSMutableString *categoryId;
@property (nonatomic, retain) NSMutableString *paramName;
@property (nonatomic, retain) NSMutableString *param;




- (IBAction)showLeftMenuPressed:(id)sender;
@end

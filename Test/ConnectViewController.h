//
//  ConnectViewController.h
//  Test
//
//  Created by Roman on 24.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConnectViewController : UIViewController <NSXMLParserDelegate>

@property (nonatomic, retain) NSMutableData *rssData;




@property (nonatomic, retain) NSMutableArray *categories;

@property (nonatomic, retain) NSString * currentElement;
@property (nonatomic, retain) NSMutableString *currentTitle;
@property (nonatomic, retain) NSMutableString *id_category;
@property (nonatomic, retain) NSMutableString *category;

@end

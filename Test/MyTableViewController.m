//
//  MyTableViewController.m
//  Test
//
//  Created by Roman on 24.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import "MyTableViewController.h"
#import "MFSideMenu.h"
#import "DishListTableViewController.h"
@interface MyTableViewController ()
@property (retain) NSString *plistFileName;
@property (retain) NSString *plistFileNameOffers;

@property (retain) NSString *id_cat;

@end

@implementation MyTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{

    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
  
    
}

-(void) loadData
{
    
    
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Categories.plist"];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
    
        UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:@"Ожидайте"
                                                           message:@"Пожалуйста подождите загрузки базы каталога."
                                                          delegate:self cancelButtonTitle:@"ОК"
                                                 otherButtonTitles:nil];
        alertView.alertViewStyle = UIAlertActionStyleDefault;
        [alertView show];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        NSURL *url = [NSURL URLWithString:@"http://ufa.farfor.ru/getyml/?key=ukAXxeJYZN"]; //ссылка на XML файл
        NSURLRequest *myReqest = [NSURLRequest requestWithURL:url cachePolicy:      NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0]; //запрос
        
        NSURLConnection *myConnection = [[NSURLConnection alloc] initWithRequest:myReqest delegate:self];
        
        if (myConnection)
        {
            self.rssData = [NSMutableData data];
            NSLog(@"Connection yes");
        }
        else
        {
            NSLog(@"Connection failed");
        }
        
        }
else {
        NSLog(@"File already exists");
        
        NSLog(@"Yes file");
        NSLog(@"plist file path: %@", plistPath);
        self.plistFileName = plistPath;
        _categoriesList = [[NSArray alloc] initWithContentsOfFile: plistPath];
        [self.categoryListTableView reloadData];
   }
    
}

- (IBAction)showLeftMenuPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _categoriesList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *newsItem = [_categoriesList objectAtIndex:indexPath.row];
    
    
    cell.textLabel.text = [newsItem objectForKey:@"title"];
    NSString *indexPathRow = [newsItem objectForKey:@"id"];
    indexPathRow = [indexPathRow stringByAppendingString:@".png"];
    cell.imageView.image = [UIImage imageNamed:indexPathRow]; 
    
    return cell;
    
    
}


-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Категории товаров:";
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DishListTableViewController *myVC = [segue destinationViewController];
    UITableViewCell *cell = (UITableViewCell*) sender;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    myVC.nameCategory = [[_categoriesList objectAtIndex:indexPath.row] objectForKey:@"title"];
    myVC.idCategory = [[_categoriesList objectAtIndex:indexPath.row] objectForKey:@"id"];
}



- (void)connection:(NSURLConnection *)myConnection didReceiveData:(NSData *)data {
    [_rssData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *result = [[NSString alloc] initWithData:_rssData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",result);
    
    self.categories = [NSMutableArray array];
    self.offers = [NSMutableArray array];
    NSXMLParser *rssParser = [[NSXMLParser alloc] initWithData:_rssData];
    
    rssParser.delegate = self;
    [rssParser parse];
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    NSLog(@"%@", error);
    
}


-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    
    self.currentElement = elementName;
    //self.id_category = nil;
    if ([elementName isEqual:@"category"])
    {
        NSArray* arr = [attributeDict allValues];
        NSString* str = [arr objectAtIndex:0];
        self.id_cat = [NSMutableString stringWithFormat:@"%@", str];
        _currentTitle = [NSMutableString string];
        _id_category = [NSMutableString string];
        _category = [NSMutableString string];
    }
    else if ([elementName isEqual:@"offer"])
    {
        NSArray* arr = [attributeDict allValues];
        NSString* str = [arr objectAtIndex:0];
        _offer_id = [NSMutableString stringWithFormat:@"%@", str];
        _url = [NSMutableString string];
        _name = [NSMutableString string];
        _price = [NSMutableString string];
        _offer_description = [NSMutableString string];
        _picture = [NSMutableString string];
        _categoryId = [NSMutableString string];
        _paramName = [NSMutableString string];
        _param = [NSMutableString string];
    }
    else if ([elementName isEqual:@"param"])
    {
        NSArray* arr = [attributeDict allValues];
        NSString* str = [arr objectAtIndex:0];
        _paramName = [NSMutableString stringWithFormat:@"%@: ", str];
    }
    
    
}


-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([_currentElement isEqualToString:@"category"]) {
        [_currentTitle appendString:string];
        [_id_category appendString:_id_cat];
        
    }
    else if ([_currentElement isEqualToString:@"offer"]) {
        [_offer_id appendString:string];
        
    }
    else if ([_currentElement isEqualToString:@"url"]) {
       if ([string rangeOfString:@"\n"].location == NSNotFound){
        [_url appendString:string];
       }
    }
    else if ([_currentElement isEqualToString:@"name"]) {
        [_name appendString:string];
    }
    else if ([_currentElement isEqualToString:@"price"]) {
        [_price appendString:string];
    }
    else if ([_currentElement isEqualToString:@"description"]) {
        [_offer_description appendString:string];
    }
    else if ([_currentElement isEqualToString:@"picture"]) {
       if ([string rangeOfString:@"\n"].location == NSNotFound){
        [_picture appendString:string];
       }
    }
    else if ([_currentElement isEqualToString:@"categoryId"]) {
        [_categoryId appendString:string];
    }
    else if ([_currentElement isEqualToString:@"param"]) {
        if ([string rangeOfString:@"\n"].location == NSNotFound){
        [_param appendString:_paramName];
        [_param appendString:string];
        _paramName = @"";
        }
        else
        {
            [_param appendString:_paramName];
            [_param appendString:@"\n"];
            _paramName = @"";

        }
    }
    
    
    
    
    
}

-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"category"])
    {
        NSDictionary *newsItem = [NSDictionary dictionaryWithObjectsAndKeys:
                                  _currentTitle, @"title",
                                  _id_category, @"id",
                                  _category, @"category" ,nil];
        [_categories addObject:newsItem];
        _currentTitle = nil;
        _id_category = nil;
        _currentElement = nil;
        _category = nil;
    }
    else if ([elementName isEqualToString:@"offer"])
    {
        NSDictionary *newsItem = [NSDictionary dictionaryWithObjectsAndKeys:
                                  _offer_id, @"id",
                                  _url, @"url",
                                  _name, @"name",
                                  _price, @"price",
                                  _offer_description, @"description",
                                  _picture, @"picture",
                                  _categoryId, @"categoryId",
                                  _param, @"param",nil];
        [_offers addObject:newsItem];
        _currentTitle = nil;
        _offer_id = nil;
        _currentElement = nil;
        _name = nil;
        _url = nil;
        _price = nil;
        _offer_description = nil;
        _picture = nil;
        _categoryId = nil;
        _param = nil;
    }
    
    
}

-(void) parserDidEndDocument:(NSXMLParser *)parser
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    // [self.tableView reloadData];
    
    // _categories
    
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Categories.plist"];
     NSString *plistPathOffers = [rootPath stringByAppendingPathComponent:@"Offers.plist"];
    _plistFileName = plistPath;
    _plistFileNameOffers = plistPathOffers;
    NSLog(@"plist file path: %@", plistPath);
    
    
    
    
    NSError *error = nil;
    
    NSData *representation = [NSPropertyListSerialization dataWithPropertyList:_categories format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
    if (!error)
    {
        BOOL ok = [representation writeToFile:self.plistFileName atomically:YES];
        if (ok)
        {
            NSLog(@"ok!");
        }
        else
        {
            NSLog(@"error writing to file: %@",self.plistFileName);
        }
    }
    else
    {
        NSLog(@"error: %@", error);
    }
    NSData *plistData = [NSData dataWithContentsOfFile:self.plistFileName];
    if (!plistData)
    {
        NSLog(@"error reading from file: %@", self.plistFileName);
        return;
    }
    NSPropertyListFormat format;
    error = nil;
    id plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
    if (!error)
    {
        NSMutableDictionary *root = plist;
        //NSLog(@"loaded data:\n%@", root);
    }
    else
    {
        NSLog(@"error: %@", error);
    }
    
    
    NSData *representationOffers = [NSPropertyListSerialization dataWithPropertyList:_offers format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
    if (!error)
    {
        BOOL ok = [representationOffers writeToFile:_plistFileNameOffers atomically:YES];
        if (ok)
        {
            NSLog(@"ok!");
        }
        else
        {
            NSLog(@"error writing to file: %@",self.plistFileNameOffers);
        }
    }
    else
    {
        NSLog(@"error: %@", error);
    }
    NSData *plistDataOffers = [NSData dataWithContentsOfFile:_plistFileNameOffers];
    if (!plistDataOffers)
    {
        NSLog(@"error reading from file: %@", _plistFileNameOffers);
        return;
    }

    plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
    if (!error)
    {
        NSMutableDictionary *rootOffes = plist;
        //NSLog(@"loaded data:\n%@", root);
    }
    else
    {
        NSLog(@"error: %@", error);
    }
    
    
    
    
    [self loadData];
    
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"%@", parseError);
}



@end

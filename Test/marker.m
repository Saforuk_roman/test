//
//  marker.m
//  Test
//
//  Created by Roman on 27.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import "marker.h"

@implementation marker


#pragma mark MKAnnotation
- (CLLocationCoordinate2D) coordinate
{
    return CLLocationCoordinate2DMake(53.915824,27.584636);
}

- (NSString *) title
{
    return @"Точка назначения";
}

@end

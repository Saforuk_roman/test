//
//  AppDelegate.h
//  Test
//
//  Created by Roman on 24.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


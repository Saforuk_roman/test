//
//  CardDishesViewController.h
//  Test
//
//  Created by Roman on 26.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardDishesViewController : UIViewController


@property (nonatomic, retain) NSString *idOffer;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *offerDescription;
@property (nonatomic, retain) NSString *picture;
@property (nonatomic, retain) NSString *categoryId;
@property (nonatomic, retain) NSString *param;
@property (nonatomic, retain) NSString *strPriceWeight;


@end

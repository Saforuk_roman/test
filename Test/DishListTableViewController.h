//
//  DishListTableViewController.h
//  Test
//
//  Created by Roman on 26.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DishListTableViewController : UITableViewController

@property (nonatomic, retain) NSString *nameCategory;
@property (nonatomic, retain) NSString *idCategory;
@end

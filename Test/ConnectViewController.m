//
//  ConnectViewController.m
//  Test
//
//  Created by Roman on 24.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import "ConnectViewController.h"
#import "MyTableViewController.h"
@interface ConnectViewController ()
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (retain) NSString *plistFileName;
@property (retain) NSString *id_cat;

@end

@implementation ConnectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Categories.plist"];
    
    
    

    
    if (!plistPath) {
        NSLog(@"No file");
        _nextButton.enabled = NO;
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        NSURL *url = [NSURL URLWithString:@"http://ufa.farfor.ru/getyml/?key=ukAXxeJYZN"]; //ссылка на XML файл
        NSURLRequest *myReqest = [NSURLRequest requestWithURL:url cachePolicy:      NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0]; //запрос
        
        NSURLConnection *myConnection = [[NSURLConnection alloc] initWithRequest:myReqest delegate:self];
        
        if (myConnection)
        {
            self.rssData = [NSMutableData data];
            NSLog(@"Connection yes");
        }
        else
        {
            NSLog(@"Connection failed");
        }

    }
    else
    {
        NSLog(@"Yes file");
        [_nextButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)connection:(NSURLConnection *)myConnection didReceiveData:(NSData *)data {
    [_rssData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *result = [[NSString alloc] initWithData:_rssData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",result);
    
    self.categories = [NSMutableArray array];
    NSXMLParser *rssParser = [[NSXMLParser alloc] initWithData:_rssData];
    
    rssParser.delegate = self;
    [rssParser parse];
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    NSLog(@"%@", error);
    
}


-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
   
    self.currentElement = elementName;
    //self.id_category = nil;
    if ([elementName isEqual:@"category"])
    {
        NSArray* arr = [attributeDict allValues];
        NSString* str = [arr objectAtIndex:0];
        self.id_cat = [NSMutableString stringWithFormat:@"%@", str];
        _currentTitle = [NSMutableString string];
        _id_category = [NSMutableString string];
        _category = [NSMutableString string];
    }
}


-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([_currentElement isEqualToString:@"category"]) {
        [_currentTitle appendString:string];
        [_id_category appendString:_id_cat];
        
    }
  
    
    
    
    
}

-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"category"])
    {
        NSDictionary *newsItem = [NSDictionary dictionaryWithObjectsAndKeys:
                                  _currentTitle, @"title",
                                  _id_category, @"id",
                                  _category, @"category" ,nil];
        [_categories addObject:newsItem];
        self.currentTitle = nil;
        self.id_category = nil;
        self.currentElement = nil;
        self.category = nil;
    }
    
}

-(void) parserDidEndDocument:(NSXMLParser *)parser
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    // [self.tableView reloadData];
    
    // _categories
    
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Categories.plist"];
    
    self.plistFileName = plistPath;
    NSLog(@"plist file path: %@", plistPath);
    
    
    
    
    NSError *error = nil;
    
    NSData *representation = [NSPropertyListSerialization dataWithPropertyList:_categories format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
    if (!error)
    {
        BOOL ok = [representation writeToFile:self.plistFileName atomically:YES];
        if (ok)
        {
            NSLog(@"ok!");
        }
        else
        {
            NSLog(@"error writing to file: %@",self.plistFileName);
        }
    }
    else
    {
        NSLog(@"error: %@", error);
    }
    NSData *plistData = [NSData dataWithContentsOfFile:self.plistFileName];
    if (!plistData)
    {
        NSLog(@"error reading from file: %@", self.plistFileName);
        return;
    }
    NSPropertyListFormat format;
    error = nil;
    id plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
    if (!error)
    {
        NSMutableDictionary *root = plist;
        //NSLog(@"loaded data:\n%@", root);
    }
    else
    {
        NSLog(@"error: %@", error);
    }
    
    
    
    [_nextButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    _nextButton.enabled = YES;
    

}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"%@", parseError);
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
   
    MyTableViewController *myVC = [segue destinationViewController];
 
//    
//    myVC.categoriesList = _categories;
//    myVC.nameFileList = _plistFileName;

}




@end

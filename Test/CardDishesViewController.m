//
//  CardDishesViewController.m
//  Test
//
//  Created by Roman on 26.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import "CardDishesViewController.h"

@interface CardDishesViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageCard;
@property (weak, nonatomic) IBOutlet UILabel *labelCard;
@property (weak, nonatomic) IBOutlet UITextView *textViewCard;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *downloadImageActivity;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

@end

@implementation CardDishesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _labelCard.text = _name;
    _labelPrice.text = _strPriceWeight;
    _textViewCard.text = _offerDescription;
    NSString *dopSimv = @"          ";
    _param = [_param stringByAppendingString:dopSimv];
    [_downloadImageActivity startAnimating];
    _downloadImageActivity.alpha = 1.0;
    _detailTextView.text = _param;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
   
    
    NSURL *url = [NSURL URLWithString:_picture];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{

            [_downloadImageActivity stopAnimating];
            _downloadImageActivity.alpha = 0.0;
            _imageCard.image = [UIImage imageWithData:imageData];
            _imageCard.alpha = 0.0;
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationCurve:1.0];
            [UIView setAnimationDelay:0.5];
            [UIView setAnimationCurve:UIViewAnimationCurveLinear];

            _imageCard.alpha = 1.0;

            [UIView commitAnimations];
        });
    });
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

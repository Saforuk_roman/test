//
//  DishListTableViewController.m
//  Test
//
//  Created by Roman on 26.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import "DishListTableViewController.h"
#import "CardDishesViewController.h"
@interface DishListTableViewController ()
@property (nonatomic, retain) NSMutableArray *offersList;
@property (nonatomic, retain) NSMutableArray *listOfDishes;

@end

@implementation DishListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title=@"Cписок блюд";
    _listOfDishes = [NSMutableArray array];
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Offers.plist"];
    _offersList = [[NSArray alloc] initWithContentsOfFile:plistPath];
   // NSLog(@"%@", _idCategory);
    
    for (int i = 0; i<_offersList.count; i++) {
        int strInt = [[[_offersList objectAtIndex:i] objectForKey:@"categoryId"] intValue];
        // NSLog(@"%i",strInt);
        int idCat = [_idCategory intValue];
        if ( strInt == idCat) {
            NSLog(@"%@",[[_offersList objectAtIndex:i] objectForKey:@"id"]);
            [_listOfDishes addObject:[_offersList objectAtIndex:i]];
        }
        
    }
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"    %@:",_nameCategory];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _listOfDishes.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *newsItem = [_listOfDishes objectAtIndex:indexPath.row];
    
    
    cell.textLabel.text = [newsItem objectForKey:@"name"];
    NSString *strWeight = [newsItem objectForKey:@"param"];
    if ([strWeight  isEqual: @""]) {
       cell.detailTextLabel.text = [NSString stringWithFormat:@"Цена: %.2f", [[newsItem objectForKey:@"price" ] floatValue]];

    }
    else {
        NSRange range = [strWeight rangeOfString:@"\n"];
        NSString *newStrWeight = [strWeight substringToIndex:range.location];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Цена: %.2f, %@", [[newsItem objectForKey:@"price" ] floatValue],newStrWeight];
    }


    NSLog([newsItem objectForKey:@"picture"]);
    NSURL *imagePostURL = [NSURL URLWithString:[newsItem objectForKey:@"picture"]];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imagePostURL];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        UIImage *postImage = [UIImage  imageWithData:data];
        
        CGFloat widthScale = 60.f / postImage.size.width;
        CGFloat heightScale = 40.f / postImage.size.height;
        
        cell.imageView.transform = CGAffineTransformMakeScale(widthScale, heightScale);
        cell.imageView.image = postImage;
        [cell setNeedsLayout];
    }];
    
    return cell;
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    CardDishesViewController *myVC = [segue destinationViewController];
    UITableViewCell *cell = (UITableViewCell*) sender;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    myVC.idOffer = [[_listOfDishes objectAtIndex:indexPath.row] objectForKey:@"id"];
    myVC.url = [[_listOfDishes objectAtIndex:indexPath.row] objectForKey:@"url"];
    myVC.name = [[_listOfDishes objectAtIndex:indexPath.row] objectForKey:@"name"];
    myVC.price = [[_listOfDishes objectAtIndex:indexPath.row] objectForKey:@"price"];
    myVC.offerDescription = [[_listOfDishes objectAtIndex:indexPath.row] objectForKey:@"description"];
    myVC.picture = [[_listOfDishes objectAtIndex:indexPath.row] objectForKey:@"picture"];
    myVC.categoryId = [[_listOfDishes objectAtIndex:indexPath.row] objectForKey:@"categoryId"];
    myVC.param = [[_listOfDishes objectAtIndex:indexPath.row] objectForKey:@"param"];
    myVC.strPriceWeight = cell.detailTextLabel.text;
}


@end

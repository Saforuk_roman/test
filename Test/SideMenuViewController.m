//
//  ConnectViewController.m
//  Test
//
//  Created by Roman on 24.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import "SideMenuViewController.h"
#import "MFSideMenu.h"
#import "MyTableViewController.h"
#import "InfoViewController.h"
#import "ConnectViewController.h"
@implementation SideMenuViewController

#pragma mark -
#pragma mark - UITableViewDataSource

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [NSString stringWithFormat:@"Section %d", section];
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Каталог";
        
    }
    else if (indexPath.row == 1)
    {
        cell.textLabel.text = @"Контакты";
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        MyTableViewController *demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"myTableViewController"];
        demoViewController.title = @"Каталог";
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoViewController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        

    }
    else if (indexPath.row == 1)
    {
        
       
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil]; // Имя вашей storyboard, обычно так и есть - MainStoryboard.
        
    InfoViewController *infoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoViewController"];
        
        
        
        [self presentViewController:infoViewController animated:YES completion:nil];
    }
}

@end
//
//  InfoViewController.m
//  Test
//
//  Created by Roman on 25.12.15.
//  Copyright © 2015 Roman. All rights reserved.
//

#import "InfoViewController.h"
#import "marker.h"
@interface InfoViewController ()
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _locationManager = [[CLLocationManager alloc] init];
    // check before requesting, otherwise it might crash in older version
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        
        [_locationManager requestWhenInUseAuthorization];
        // [locationManager requestAlwaysAuthorization];
        
    }
    _mapView.showsUserLocation = YES;
    _mapView.mapType = MKMapTypeStandard;
    [_mapView setDelegate:self];
    
    marker *m = [[marker alloc] init];
    [_mapView addAnnotation:m];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated
{
    
//    CLLocationManager * locationManager = [[CLLocationManager alloc] init];
//    // check before requesting, otherwise it might crash in older version
//    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
//        
//        [locationManager requestWhenInUseAuthorization];
//       // [locationManager requestAlwaysAuthorization];
//        
//    }
    MKCoordinateRegion region = MKCoordinateRegionMake(CLLocationCoordinate2DMake(53.887124, 27.516298),
                                                       MKCoordinateSpanMake(0.1,0.1));
    [_mapView setRegion:region animated:YES];
}
- (IBAction)read:(id)sender
{
//    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
//    
//    // If the status is denied or only granted for when in use, display an alert
//    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
//        NSString *title;
//        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
//        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
//        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
//                                                            message:message
//                                                           delegate:self
//                                                  cancelButtonTitle:@"Cancel"
//                                                  otherButtonTitles:@"Settings", nil];
//        [alertView show];
//    }
//    // The user has not enabled any location services. Request background authorization.
//    else if (status == kCLAuthorizationStatusNotDetermined) {
//        [self.locationManager requestAlwaysAuthorization];
//    }
    
   

    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // Send the user to the Settings for this app
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    }
}
- (IBAction)ckickBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
//    MKCoordinateRegion region;
//    MKCoordinateSpan span;
//    span.latitudeDelta = 0.055;
//    span.longitudeDelta = 0.055;
//    CLLocationCoordinate2D location;
//    location.latitude = aUserLocation.coordinate.latitude;
//    location.longitude = aUserLocation.coordinate.longitude;
//    region.span = span;
//    region.center = location;
//    [aMapView setRegion:region animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
